//
//  ViewController.h
//  Alert
//
//  Created by HorieTsubasa on 2019/04/04.
//  Copyright © 2019 HorieTsubasa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController

// 通知発行
@property (weak, nonatomic) IBOutlet UIButton *Button;
@property (weak, nonatomic) IBOutlet UITextView *Label;

// 音声再生
@property(nonatomic) AVAudioPlayer *player;
@property (weak, nonatomic) IBOutlet UITextView *Label2;
@property (weak, nonatomic) IBOutlet UIButton *Button2;
@end

