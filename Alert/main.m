//
//  main.m
//  Alert
//
//  Created by HorieTsubasa on 2019/04/04.
//  Copyright © 2019 HorieTsubasa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
